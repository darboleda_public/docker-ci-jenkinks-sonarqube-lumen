# Lumen PHP JWT login wit CI

## START AND INSTALL JENKINS:
```sh
docker run -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home jenkins/jenkins
```

## SET CONFIGURATION FOR SONAR: 
```sh
sudo sysctl -w vm.max_map_count=262144 
sudo sysctl -w fs.file-max=65536 
sudo ulimit -n 65536 
sudo ulimit -u 4096 
```

## Start sonar
```sh
docker run --name sonarqube -p 9000:9000 -e sonar.jdbc.url=jdbc:postgresql://172.17.0.1:5432/sonarqube -e sonar.jdbc.username=sonar -e sonar.jdbc.password=your-strong-password-here  -v sonarqube_conf:/opt/sonarqube/conf -v sonarqube_extensions:/opt/sonarqube/extensions -v sonarqube_logs:/opt/sonarqube/logs -v sonarqube_data:/opt/sonarqube/data sonarqube
```

## INSTALL RUNNER IN JENKINS
```sh
docker ps 
docker exec -it 630605b72d88 bash 
cd /var/jenkins_home 
wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.2.0.1873-linux.zip
unzip sonar-scanner-cli-4.2.0.1873-linux.zip 
```

## Pipeline example
```sh
pipeline {
    agent any
    stages {
        stage('checkout from gitlab') {
            steps {
                git branch: 'master',
		        credentialsId: 'gitab-darboleda',
		        url: 'https://gitlab.com/darboleda_public/lumen-JWT-login.git'
            }
        }
        
        stage('Code Quality'){
            steps {
                script {
                    def scannerHome = tool 'sonarqube';  //global tool configuration http://172.17.0.1:8080/configureTools/
        			   withSonarQubeEnv("sonarque-container") { //sonarqube server http://172.17.0.1:8080/configure
        			   sh "${tool("sonarqube")}/bin/sonar-scanner \
        							-Dsonar.projectKey=lumen-ci \
                                    -Dsonar.sources=. \
                                    -Dsonar.language=php \
									-Dsonar.exclusions=vendor \
                                    -Dsonar.sourceEncoding=UTF-8 \
                                    -Dsonar.host.url=http://172.17.0.1:9000 \
                                    -Dsonar.login=xxxxxxxxxxxxxx"
        				   }
                }
            }
        }
    }
}
```
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemFile extends Model
{		
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items_files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'items_id', 'id'
    ];

    /**
     * Get item of the otitem
     * @return App\Item
     */
    public function item()
    {
        return $this->belongsTo('App\Item','id','items_id');
    }
}
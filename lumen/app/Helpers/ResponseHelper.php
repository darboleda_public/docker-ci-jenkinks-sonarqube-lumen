<?php

namespace App\Helpers;

class ResponseHelper
{   
    const KEY_HTTP_CODE = 'http_code';
    const KEY_CODE = 'code';
    const KEY_ERROR = 'error';
    const KEY_MSG = 'msg';

    const ERRORS = [
        /*JWT Auth errors*/
        'NOT_ALLOWED' => [
            self::KEY_ERROR => 'Not allowed.',
            self::KEY_CODE => 1000,
            self::KEY_HTTP_CODE => 401,
        ],
        'TOKEN_NOT_PROVIDED' => [
            self::KEY_ERROR => 'Token not provided.',
            self::KEY_CODE => 1001,
            self::KEY_HTTP_CODE => 401,
        ],
        'TOKEN_EXPIRED' => [
            self::KEY_ERROR => 'Token is expired.',
            self::KEY_CODE => 1002,
            self::KEY_HTTP_CODE => 400,
        ],
        'TOKEN_INVALID' => [
            self::KEY_ERROR => 'Token invalid.',
            self::KEY_CODE => 1003,
            self::KEY_HTTP_CODE => 400,
        ],

        /*User errors*/
        'USER_NOT_EXISTS' => [
            self::KEY_ERROR => 'Invalid user or password.',
            self::KEY_CODE => 2000,
            self::KEY_HTTP_CODE => 400,
        ],
        'USER_EXISTS' => [
            self::KEY_ERROR => 'Email exists.',
            self::KEY_CODE => 2001,
            self::KEY_HTTP_CODE => 500,
        ],
        'USER_DISABLED' => [
            self::KEY_ERROR => 'The user is disabled.',
            self::KEY_CODE => 2002,
            self::KEY_HTTP_CODE => 400,
        ],
    ];
    
    /**
     * Get error reponse
     * 
     * @param  string $error
     * @return Mixed
     */
    public static function getErrorResponse($error)
    {
        return response()->json(
            self::getErrorResponseAsArray($error),
            self::getErrorResponseAsArray($error)[self::KEY_HTTP_CODE]
        );
    }

    /**
     * Get json response
     * 
     * @param  array $response
     * @param  string $key class and method
     * @return Mixed
     */
    public static function getResponse($response, $key)
    {   
        $key = str_replace('::', '_', $key);        
        $arrKey = explode('\\', $key);
        $key = strtolower(str_replace('Controller', '', $arrKey[count($arrKey)-1]));
        $response[self::KEY_HTTP_CODE] = 200;
        $response[self::KEY_MSG] = $key.'_ok';
        return response()->json($response, 200);
    }

    /**
     * Get array error response
     * 
     * @param  string $error
     * @return array
     */
    public static function getErrorResponseAsArray($error)
    {
        return self::ERRORS[$error];
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Helpers\Errors;

class Controller extends BaseController
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * Process the request, keep only the valid params
     * @param array $keepOnly
     * @return array $postFields
     */
    protected function processRequest($keepOnly = [])
    {
        $postFields = $this->request->all();
        $cleanRequest = [];
        foreach ($keepOnly as $key => $value) {
            if (isset($postFields[$key])) {
                $cleanRequest[$key] = $postFields[$key];
            }                
        }
        $postFields = $cleanRequest;                
        return $postFields;
    }
}

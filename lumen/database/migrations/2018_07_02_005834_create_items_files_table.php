<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsFilesTable extends Migration
{   
    private $table = 'items_files';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::dropIfExists($this->table);
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('items_id')->unsigned();
            $table->string('file');
            $table->timestamps();
        });

        Schema::table($this->table, function(Blueprint $table) {
           $table->foreign('items_id')
              ->references('id')->on('items')
              ->onDelete('cascade');            
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}

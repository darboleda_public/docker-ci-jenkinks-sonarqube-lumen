<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{   
    private $table = 'items';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::dropIfExists($this->table);
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->integer('users_id')->unsigned();
            $table->timestamps();
        });

        Schema::table($this->table, function(Blueprint $table) {
           $table->foreign('users_id')
              ->references('id')->on('users')
              ->onDelete('cascade');

            $table->index('title');
            $table->index('description');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}

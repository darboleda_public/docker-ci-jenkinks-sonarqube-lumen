<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'TestUser',
            'email' => 'test_user@gmail.com',
            'password' => password_hash('12345', PASSWORD_DEFAULT),
            'created_at' => Carbon::now()->toDateTimeString()
        ]);

        DB::table('users')->insert([
            'name' => 'TestUser2',
            'email' => 'test_user2@gmail.com',
            'password' => password_hash('12345', PASSWORD_DEFAULT),
            'created_at' => Carbon::now()->toDateTimeString()
        ]);
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->options('items', function() {			
	return response()->json(['status' => 'ok']);
});

$router->options('messages', function() {			
	return response()->json(['status' => 'ok']);
});

$router->options('messagesresume', function() {			
	return response()->json(['status' => 'ok']);
});

$router->options('myitems', function() {			
	return response()->json(['status' => 'ok']);
});

$router->options('edititems', function() {			
	return response()->json(['status' => 'ok']);
});

$router->get(
	'items',
	[
		'uses' => 'ItemController@getItems'
	]
);

$router->group(['prefix' => 'auth'], function () use ($router) {
	
	$router->options('login', function() {			
		return response()->json(['status' => 'ok']);
	});

	$router->post(
		'login',
		[
			'uses' => 'AuthController@authenticate'
		]
	);

	$router->options('register', function() {			
		return response()->json(['status' => 'ok']);
	});

	$router->post(
		'register',
		[
			'uses' => 'AuthController@register'
		]
	);
});

$router->group(
	['middleware' => 'auth'],
	function() use ($router) {
		
		$router->get('users', function() {
			$users = \App\User::all();
			return response()->json($users);
		});		

		$router->post(
			'items',
			[
				'uses' => 'ItemController@addItem'
			]
		);

		$router->get(
			'myitems',
			[
				'uses' => 'ItemController@getMyItems'
			]
		);

		$router->post( //Quick fix for ionic, problems with put
			'edititems',
			[
				'uses' => 'ItemController@updateItem'
			]
		);

		$router->post(
			'itemupload',
			[
				'uses' => 'ItemController@upload'
			]
		);

		$router->post(
			'messages',
			[
				'uses' => 'MessageController@addMessage'
			]
		);

		$router->get(
			'messages',
			[
				'uses' => 'MessageController@getMyMessages'
			]
		);

		$router->get(
			'messagesresume',
			[
				'uses' => 'MessageController@getMessagesResume'
			]
		);		
	}
);
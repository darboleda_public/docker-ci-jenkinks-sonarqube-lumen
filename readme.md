# Lumen PHP JWT login wit CI

1. composer install
2. configure your .env with .env.example, take care of the JWT_* fields in .env
3. php artisan migrate --seed
4. vendor\bin\phpunit tests

## To generate database data
```sh
php artisan migrate --seed
```

## To run test
```sh
vendor\bin\phpunit tests

## Configure docker
Please follow these steps to run docker

## Start environment
To start docker environment, you must install [Docker](https://www.docker.com/get-started) and digit this command in your favorite terminal:
```sh
star_docker.sh
```
## First execution
firt get the docker image id for "docker_php" with the comand, that is the first column:
```sh
docker ps
```
then run composer install with:
```sh
docker exec -i -t DOCKER_PHP_IMAGE_ID "cd .. && composer install"
```

then run database seed with:
```sh
docker exec -i -t DOCKER_PHP_IMAGE_ID php ../artisan migrate --seed
```

When the process finished you can open your browser and digit http://localhost:8181 to se you app works!

```

## Stop environment
To stop your environment you must digit this command in your favorite terminal:
```sh
stop_docker.sh
```

## Debugger
If you use PhpStorm you must configure your IDE:
- open `Preferences | Languages & Frameworks | PHP | Servers`
- add new server named `localhost`
- set host to `localhost`
- set port to `8181`
- enable `use path mapping`
- configure your absolute path
- your Lumen/Laravel app root => `/var/www`
- your Lumen/Laravel app /app root => `/var/www/app`
- your Lumen/Laravel app /public root => `/var/www/public`
- open `Preferences | Languages & Frameworks | PHP` => `CLI interpreter`
- add new CLI from Docker => `docker_php:latest`
